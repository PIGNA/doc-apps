
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="container">
	<div class="row centered-form">
		<div class="col-xs-12 col-sm-8 col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">identification</h3>
				</div>
				<div class="panel-body">
					<form role="form" method="get" action="loginpage">

						<div class="form-group">
							<input type="ID" name="ID" id="ID" class="form-control input-sm"
								placeholder="ID or Email Address">
						</div>

						<div class="form-group">
							<input type="password" name="password" id="password"
								class="form-control input-sm" placeholder="Password">
						</div>

						<input type="submit" value="submit"
							class="btn btn-info btn-block">

					</form>
				</div>
			</div>
		</div>
	</div>
</div>