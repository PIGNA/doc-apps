package com.my.doc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SigninController {
	@RequestMapping(method = RequestMethod.GET, path ="/signinpage")
	public String SigninPage() {
	
		return "signin";

	}
}
