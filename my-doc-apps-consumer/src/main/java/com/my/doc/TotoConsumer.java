package com.my.doc;

public class TotoConsumer {
	String name = "toto";
	int age = 20;

	public TotoConsumer() {
		super();
	}

	public TotoConsumer(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Toto [name=" + name + ", age=" + age + "]";
	}

}
