package com.my.doc.toto;

public class TotoBatch {
	String name = "toto";
	int age = 20;
	
	public TotoBatch() {
		super();
	}
	public TotoBatch(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Toto [name=" + name + ", age=" + age + "]";
	}	

}
