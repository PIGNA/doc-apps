package com.my.doc;

import static org.junit.Assert.*;

import org.junit.Test;

public class TotoConsumerTest {

	TotoConsumer toto = new TotoConsumer();
	@Test
	public void testGetName() {
		assertSame("toto", toto.getName());
	}

	@Test
	public void testGetAge() {
		assertSame(20, toto.getAge());
	}

}
