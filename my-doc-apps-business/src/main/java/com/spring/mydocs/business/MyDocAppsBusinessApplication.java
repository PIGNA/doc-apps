package com.spring.mydocs.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyDocAppsBusinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyDocAppsBusinessApplication.class, args);
	}

}
