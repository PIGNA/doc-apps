<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">

	<!-- Brand -->
	<a class="navbar-brand" href="aboutpage">${application.name}</a>

	<!-- Toggler/collapsibe Button -->
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#collapsibleNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<!-- Navbar links -->
	<div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item"><a class="nav-link" href="homepage">home</a></li>
			<li class="nav-item"><a class="nav-link" href="aboutpage">about</a>
			</li>
		</ul>
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<form method="post" action="signin">
					<a class="nav-link" href="loginpage">login</a>
					<input type="hidden" name="signin" value="signin"/>
				</form>
			</li>
			<li class="nav-item"><a class="nav-link" href="signinpage">signin</a></li>

		</ul>
	</div>
</nav>


