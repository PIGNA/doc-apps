package com.my.doc.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.my.doc.TotoModel;

public class TotoModelTest {
TotoModel toto = new TotoModel();
	@Test
	public void testGetName() {
		assertSame("toto", toto.getName());
	}

	@Test
	public void testGetAge() {
		assertSame(20, toto.getAge());
	}

}
