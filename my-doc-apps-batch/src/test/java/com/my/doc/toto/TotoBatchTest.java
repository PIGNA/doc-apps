package com.my.doc.toto;

import static org.junit.Assert.*;

import org.junit.Test;


public class TotoBatchTest {
		TotoBatch toto = new TotoBatch();
	@Test
	public void testGetName() {
		assertSame("toto", toto.getName());
	}

	@Test
	public void testGetAge() {
		assertSame(20, toto.getAge());
	}

}
